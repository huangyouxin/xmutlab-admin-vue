import {
  AccountParams,
  JobTitle,
  MenuParams,
  RoleParams,
  RolePageParams,
  MenuListGetResultModel,
  JobTitleListGetResultModel,
  AccountListGetResultModel,
  RolePageListGetResultModel,
  RoleListGetResultModel,
  Account,
  TeacherItem,
  StudentItem,
  StudentListGetResultModel,
} from './model/systemModel';
import { defHttp } from '/@/utils/http/axios';

enum Api {
  AccountList = '/admin/getList',
  IsAccountExist = '/admin/adminExist',
  RegistAdmin = '/admin/register',
  DeleteAcount = '/admin/delete',

  setRoleStatus = '/system/setRoleStatus',
  MenuList = '/system/getMenuList',
  GetAllRoleList = '/system/getAllRoleList',

  TeacherList = '/teacher/getList',
  DeleteTeacher = '/teacher/delete',
  RegistTeacher = '/teacher/regist',

  StudentList = '/student/getList',
  RegistStudent = '/student/regist',
  DeleteStudent = '/student/delete',

  Category = '/util/getCategory',
} 

export const getMenuList = (params?: MenuParams) =>
  defHttp.get<MenuListGetResultModel>({ url: Api.MenuList, params });

export const getAllRoleList = (params?: RoleParams) =>
  defHttp.get<RoleListGetResultModel>({ url: Api.GetAllRoleList, params });

export const setRoleStatus = (id: number, status: string) =>
  defHttp.post({ url: Api.setRoleStatus, params: { id, status } });



export const getAccountList = (params: AccountParams) =>
  defHttp.get<AccountListGetResultModel>({ url: Api.AccountList, params });

export const isAccountExist = (adminName?: string) =>
  defHttp.get({ url: Api.IsAccountExist, params:{adminName} }, { errorMessageMode: 'none' });

export const registAccount = (params:Account) =>
  defHttp.post({ url: Api.RegistAdmin, params});

export const deleteAccount = (adminId:string) =>
  defHttp.post({ url: Api.DeleteAcount, params:{adminId}});



export const getTeacherList = (params?: RolePageParams) =>
  defHttp.get<RolePageListGetResultModel>({ url: Api.TeacherList, params });

export const RegistTeacher = (params:TeacherItem) =>
  defHttp.post({ url: Api.RegistTeacher, params});

export const deleteTeacher = (id:string) =>
  defHttp.post({ url: Api.DeleteTeacher, params:{id}});
  

  

export const getStudentList = (params?: StudentItem) =>
  defHttp.get<StudentListGetResultModel>({ url: Api.StudentList, params });

export const RegistStudent = (params:TeacherItem) =>
  defHttp.post({ url: Api.RegistStudent, params});

export const deleteStudent = (id:string) =>
  defHttp.post({ url: Api.DeleteStudent, params:{id}});



export const getCategory = (name:string) =>
  defHttp.get({ url: Api.Category,params:{name}});