import { getAllRoleList, isAccountExist } from '/@/api/demo/system';
import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';

export const columns: BasicColumn[] = [
  {
    title: 'id',
    dataIndex: 'adminId',
    width: 120,
  },
  {
    title: '用户名称',
    dataIndex: 'adminName',
  },
  {
    title: '创建时间',
    dataIndex: 'updateTime',
  }

];

export const searchFormSchema: FormSchema[] = [
  {
    field: 'name',
    label: '用户名',
    component: 'Input',
    colProps: { span: 8 },
  }
];

export const accountFormSchema: FormSchema[] = [
  {
    field: 'adminName',
    label: '用户名',
    component: 'Input',
    helpMessage: ['本字段演示异步验证', '不能输入带有admin的用户名'],
    rules: [
      {
        required: true,
        message: '请输入用户名',
      },
      {
        validator(_, value) {
          return new Promise((resolve, reject) => {
            if(value.length > 10){
              reject('用户名字符数应小于10');
            }else{
              isAccountExist(value)
                .then(() => resolve())
                .catch((err) => {
                  reject(err.message || '验证失败');
                });
            }
          });
        },
      },
    ],
  },
  {
    field: 'adminPassword',
    label: '密码',
    component: 'InputPassword',
    required: true,
  },
];
