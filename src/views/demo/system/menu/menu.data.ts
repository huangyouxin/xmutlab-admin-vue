import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { h } from 'vue';
import { Tag } from 'ant-design-vue';
import { Icon } from '/@/components/Icon';

export const columns: BasicColumn[] = [
  {
    title: '学生姓名',
    dataIndex: 'studentName',
    width: 120,
  },
  // {
  //   title: '学历',
  //   dataIndex: 'icon',
  //   width: 50,
  //   customRender: ({ record }) => {
  //     return h(Icon, { icon: record.icon });
  //   },
  // },
  {
    title: '学生学历',
    dataIndex: 'type',
    width: 80,
  },
  {
    title: '是否毕业',
    dataIndex: 'status',
    width: 80,
  },
  {
    title: '入学年级',
    dataIndex: 'grade',
    width: 120,
  },
  {
    title: '研究方向',
    dataIndex: 'research',
    width: 200,
  },
  {
    title: '创建时间',
    dataIndex: 'createTime',
    width: 180,
  },
];

const isDir = (type: string) => type === '0';
const isMenu = (type: string) => type === '1';
const isButton = (type: string) => type === '2';

export const searchFormSchema: FormSchema[] = [
  {
    field: 'menuName',
    label: '菜单名称',
    component: 'Input',
    colProps: { span: 8 },
  },
  {
    field: 'status',
    label: '状态',
    component: 'Select',
    componentProps: {
      options: [
        { label: '启用', value: '0' },
        { label: '停用', value: '1' },
      ],
    },
    colProps: { span: 8 },
  },
];

export const formSchema: FormSchema[] = [
  {
    field: 'studentName',
    label: '学生姓名',
    component: 'Input',
  },
  {
    field: 'type',
    label: '学历',
    component: 'TreeSelect',
    required: true,
    componentProps: {
      replaceFields: {
        title: 'name',
        value: 'name',
      },
      getPopupContainer: () => document.body,
    },
  },
  {
    field: 'status',
    label: '毕业状态',
    component: 'TreeSelect',
    componentProps: {
      replaceFields: {
        title: 'name',
        value: 'name',
      },
      getPopupContainer: () => document.body,
    },
  },

  { 
    field: 'grade',
    label: '入学年级',
    component: 'TreeSelect',
    componentProps: {
      replaceFields: {
        title: 'name',
        value: 'name',
      },
      getPopupContainer: () => document.body,
    },
  },

  {
    field: 'research',
    label: '研究方向',
    component: 'TreeSelect',
    componentProps: {
      replaceFields: {
        title: 'name',
        value: 'name',
      },
      getPopupContainer: () => document.body,
    },
  },
];
