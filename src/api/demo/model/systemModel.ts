import { StringNullableChain } from 'lodash';
import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';

export type AccountParams = BasicPageParams & {
  account?: string;
  nickname?: string;
};

export type RoleParams = {
  roleName?: string;
  status?: string;
};

export type RolePageParams = BasicPageParams & RoleParams;

export type DeptParams = {
  deptName?: string;
  status?: string;
};

export type MenuParams = {
  menuName?: string;
  status?: string;
};

export interface AccountListItem {
  adminId: string;
  adminName: string;
  updateTime: string;
}

export interface DeptListItem {
  id: string;
  orderNo: string;
  createTime: string;
  remark: string;
  status: number;
}

export interface MenuListItem {
  id: string;
  orderNo: string;
  createTime: string;
  status: number;
  icon: string;
  component: string;
  permission: string;
}

export interface RoleListItem {
  id: string;
  roleName: string;
  roleValue: string;
  status: number;
  orderNo: string;
  createTime: string;
}

export interface Account {
 adminName: string;
 adminPassword: string;
}

export interface JobTitle{
  name: string;
}

export interface TeacherItem {
  id: string;
  teacherName: string;
  type: string;
  picUrl:string;
  detail:String;
  addTime:String;
  updateTime:String;
}

export interface StudentItem {
  id: string;
  studentName: string;
  type: string;
  status:string;
  grade:String;
  research:string;
  addTime:String;
  updateTime:String;
}


/**
 * @description: Request list return value
 */
export type AccountListGetResultModel = BasicFetchResult<AccountListItem>;

export type JobTitleListGetResultModel = BasicFetchResult<JobTitle>;

export type MenuListGetResultModel = BasicFetchResult<MenuListItem>;

export type RolePageListGetResultModel = BasicFetchResult<RoleListItem>;

export type RoleListGetResultModel = RoleListItem[];

export type TeacherListGetResultModel = BasicFetchResult<TeacherItem>;

export type StudentListGetResultModel = BasicFetchResult<StudentItem>;